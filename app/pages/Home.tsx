import { useState } from 'react';

const Home = () => {
  const [count, setCount] = useState(0);

  const plus = () => {
    setCount(count + 1);
  };

  const minus = () => {
    setCount(count - 1);
  };

  return (
    <div>
      <span id="count">Count = {count}</span>
      <div>
        <button id="plus" onClick={plus}>+</button>
        <button id="minus" onClick={minus}>-</button>
      </div>
    </div>
  );
};

export default Home;
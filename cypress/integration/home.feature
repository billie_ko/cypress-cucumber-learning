Feature: Plus or Minus Count

  # A Background allows you to add some context to the scenarios that follow it. 
  # It can contain one or more Given steps, 
  # which are run before each scenario, but after any Before hooks.
  Background:
    Given I open Home Page

  @foo
  Scenario: I can increment the count
    When I click the plus button
    And I click the plus button
    Then The count should be 2

  @bar
  Scenario: I can decrement the count
    When I click the plus button
    And I click the plus button
    And I click the minus button
    And I click the minus button
    Then The count should be 0
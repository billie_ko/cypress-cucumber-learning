import { Before } from '@badeball/cypress-cucumber-preprocessor';

Before(() => {
  // This hook will be executed before all scenarios.
  cy.log('>--->');
});

Before({ tags: '@foo' }, () => {
  // This hook will be executed before scenarios tagged with @foo.
  cy.log('>---> @foo');
});

Before({ tags: '@foo and @bar' }, () => {
  // This hook will be executed before scenarios tagged with @foo and @bar.
  cy.log('>---> @foo and @bar');
});

Before({ tags: '@foo or @bar' }, () => {
  // This hook will be executed before scenarios tagged with @foo or @bar.
  cy.log('>---> @foo or @bar');
});
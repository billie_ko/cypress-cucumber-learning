import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor';

Given('I open Home Page', () => {
  cy.visit('');
});

When('I click the plus button', () => {
  cy.get('#plus').click();
});

When('I click the minus button', () => {
  cy.get('#minus').click();
});

Then('The count should be {int}', (count) => {
  cy.get('#count').should('contain.text', count);
});